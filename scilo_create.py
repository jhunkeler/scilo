#!/usr/bin/env python
import argparse
import scilo

def main():
    parser = argparse.ArgumentParser(description='Generate scientific workflow structure')
    parser.add_argument('-c', '--copy-only', default=True, action='store_false', required=False,
                        help='Do not move original data')
    parser.add_argument('-o', '--output-dir', type=str, required=True,
                        help='Where to store aggregated data, caches, results')
    parser.add_argument('input_files', type=str, nargs='*',
                        help='Example: /path/to/data/*.extension')
    arguments = parser.parse_args()
    
    if not arguments.input_files:
        print('Please specify files to aggregate')
        exit(1)
    
    dataset = scilo.scilo(arguments.output_dir)
    dataset.aggregate(arguments.input_files, move=arguments.copy_only)

if __name__ == "__main__":
    main()

