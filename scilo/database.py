'''
scilo - A scientific workflow and efficiency library 
Copyright (C) 2012  Joseph Hunkeler <jhunkeler@gmail.com>

This file is part of scilo.

scilo is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

scilo is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with scilo.  If not, see <http://www.gnu.org/licenses/>.
'''
import sqlite3
class s3:
    def __init__(self, database):
        self.connection = None
        self.cursor = None
        try:
            self.connection = sqlite3.connect(database)
            self.cursor = self.connection.cursor()
        except:
            raise
            